

class FleetState{
    constructor(wf, cap, concurrency, delay) {
        this.runningCount = 0;
        this.tasks = [];
        this.workerFnc = wf
        this.isRunning = false;
        this.capacity = cap
        this.idleWorkers = []
        this.concurrency = concurrency?concurrency:1
        this.me = this
        this.d = delay;
    }

    // array of workers
    
    makeFleet(workerFunc){
        console.log("about to make fleet of " + this.concurrency)
        for(var i = 0; i < this.concurrency; i++){
            let asyncFunc = async function(t){
                workerFunc( t )
            }
            // push this asyncFunc into an array
            this.idleWorkers.push(asyncFunc);
            console.log(" num workers " + this.idleWorkers.length)

        }
    }

    incrementRunningCount() {
        this.runningCount++;
    }

    getRunningCount(){
        return this.runningCount;
    }
    addWorker(w, callback){
        this.idleWorkers.push(w)
        console.log(this.idleWorkers.length);
        if(callback){
            callback();
        }
    }
    popWorker(){
        let w = this.idleWorkers.pop();
        console.log(this.idleWorkers.length)
        return w;
    }
    push(t, callback) {
        this.tasks.push(t);
     
        while (this.tasks.length > 0){
            if(this.idleWorkers.length > 0 && this.tasks.length <= this.capacity){
                let iw = this.me.popWorker()
                iw( this.tasks.pop() ).then(this.me.addWorker(iw, callback));
                
            }
        }
        this.doEmpty();
        
        //
    }

    popTask(){
        return this.tasks.pop();
    }
    doEmpty(){
        if(this.c){
            return this.c();
        }
    }
    empty(c){
        this.c = c;
    }

    get length() {
        return this.tasks.length;
    }

    get delay(){
        return this.d
    }

    get running(){
        return this.isRunning;
    }

}

const fleet = function(worker, capacity, concurrency, delay){
    // create capac numeber of async workers
    let fs = new FleetState(worker, capacity, concurrency, delay)
    fs.makeFleet(worker)
    return fs;
}

// let f = function(t){ 
//     console.log(t)
// }

// fleet(f, 10, 10);

let f =  function(t){ 
    for(var i = 0; i < 10; i++){
        console.log(t)
    }
}

let fs = fleet(f, 10, 10);
fs.push("jones");
fs.push("rick");
fs.push("mary");
fs.push("bob");